package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.repository.IUserRepository;
import ru.nirinarkhova.tm.exception.entity.UserNotFoundException;
import ru.nirinarkhova.tm.model.User;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public Optional<User> findByLogin(final String login) {
        return entities.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return entities.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst();
    }

    @Override
    public User removeByLogin(final String login) {
        final Optional<User> user = findByLogin(login);
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}
