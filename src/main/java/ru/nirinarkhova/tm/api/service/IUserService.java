package ru.nirinarkhova.tm.api.service;

import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.model.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    Optional<User> setPassword(String userId, String password);

    Optional<User> findById(String id);

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    Optional<User> updateUser(String userId, String firstName, String lastName, String middleName);

    User removeById(String id);

    User removeByLogin(String login);

    Optional<User> lockUserByLogin(String login);

    Optional<User> unlockUserByLogin(String login);

}
