package ru.nirinarkhova.tm.api.service;

import ru.nirinarkhova.tm.api.IBusinessService;
import ru.nirinarkhova.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    Task add(String userId, String name, String description);

}

