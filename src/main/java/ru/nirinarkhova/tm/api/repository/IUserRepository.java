package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

    User removeByLogin(String login);

}

