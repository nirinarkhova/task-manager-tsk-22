package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-change-status-by-id";
    }

    @Override
    public String description() {
        return "change project status by project id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Optional<Project> project = serviceLocator.getProjectService().findById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        final Optional<Project> projectStatusUpdate = serviceLocator.getProjectService().changeStatusById(userId, id, status);
        Optional.ofNullable(projectStatusUpdate).orElseThrow(ProjectNotFoundException::new);
    }

}

