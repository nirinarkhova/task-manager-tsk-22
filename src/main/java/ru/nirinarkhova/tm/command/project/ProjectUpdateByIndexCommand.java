package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "update a project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Optional<Project> project = serviceLocator.getProjectService().findByIndex(userId, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("[ENTER PROJECT NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Optional<Project> projectUpdate = serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
        Optional.ofNullable(projectUpdate).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}

